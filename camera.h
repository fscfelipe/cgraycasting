#ifndef CAMERA_H
#define CAMERA_H

#include "ponto.h"
#include "vetor.h"
#include "ray.h"
#include "cmath"

/*-------------------------------------------------
  Definições da câmera (observador)
 *-------------------------------------------------*/
class Camera
{
public:

    Vetor ic, jc, kc;
    Ponto posicao;

    // Altura e largura da imagem (Plano de projeção)
    int larguraImg;
    int alturaImg;

    Camera();
    Camera(int imgLargura, int imgAltura);

    // Iremos lançar raios através de cada pixel da imagem
    Ray lancarRaio(int xImagem, int yImagem);
    void lookAt( Ponto ponto );
    Ponto getPos();
    void setPos(Ponto posicao);
};

#endif // CAMERA_H

/*Fonte
 * 1.https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-generating-camera-rays/generating-camera-rays
*/
