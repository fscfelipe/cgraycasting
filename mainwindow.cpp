#include "mainwindow.h"
#include "ui_mainwindow.h"



#include <chrono>
using namespace std::chrono;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    // Inicio contador runtime
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    /*----------------------------------------------------------------------*
      Métodos executados quando a janela for 'carregada'
     *----------------------------------------------------------------------*/
    definirCenario();

    carregarTeste();
    //carregarCenarioCompleto();


    renderizarCena();


    // fim contador runtime
    high_resolution_clock::time_point t2 = high_resolution_clock::now();
    auto duration = duration_cast<seconds>( t2 - t1 ).count();
    cout << "TEMPO : ";
    cout << duration << endl;
}

/*---------------------------------------------------------------------------*
  Método utilizado para definir o nosso cenário, adicionando objetos e
  fonts de iluminação no mesmo.
 *--------------------------------------------------------------------------*/
void MainWindow::definirCenario(){
    //adicionarSnowMan();

    QSize janela =  ui->graphicsView->size();
    int alturaJanela = janela.height();
    int larguraJanela = janela.width();

    //Material materialLampada(Cor(1, 1, 1),Cor(1, 1, 1),Cor(1, 1, 1),200, 1.0, 1.0, 0.05);
    //cenario.adicionaEsfera( Ponto(0,3,0), 0.1, materialLampada, true );
    //cenario.adicionaIluminacao(Ponto(0, 3,0), 1.0, 1.0, 1.0, 0.02);

    // Câmera
    camera = new Camera(larguraJanela, alturaJanela);
    camera->setPos( Ponto(5, 5, 5) );
    camera->lookAt( Ponto(0 , 0, 0) );
}

void MainWindow::carregarCenarioCompleto(){

    /*-----------------------------------------------------------------------*
      Carregando objeto e criando lista
     *-----------------------------------------------------------------------*/
    Material materialVermelho( Cor(0.2, 0.0, 0.0), Cor(1.0, 0, 0), Cor(0.2, 0.2, 0.2), 200, 1.0, 1.0, 0.5 );
    Material materialPrateado( Cor(0.19225, 0.19225, 0.19225), Cor(0.50754, 0.50754, 0.50754), Cor(0.508273, 0.508273, 0.508273), 0.4, 1.0, 1.0, 1.0 );
    Material materialLampada(Cor(1, 1, 1),Cor(1, 1, 1),Cor(1, 1, 1),200, 1.0, 1.0, 0.05);
    Material materialBrancoCinza( Cor(0.2, 0.2, 0.2), Cor(1.0, 1.0, 1.0), Cor(1.0, 1.0, 1.0), 200, 1.0, 1.0, 0.05 );

    // Carregando base = 1 segundo
    parser = new Parser("../recursos/base.obj");
    parser->carregarArquivo();
    criarObjeto(materialBrancoCinza);

    // Carregando Poste 1 = Normal = 3 segundos ,Aprimorado = 120 segundos
    parser = new Parser("../recursos/posteSimples1.obj");
        // Esfera para lâmapda poste 1
    cenario.adicionaEsfera( Ponto(4.79 , 5.5, 6.8), 0.15, materialLampada, true );
        // Lâmpada poste 1
    cenario.adicionaIluminacao(Ponto(4.79 , 5.5, 6.8), 0.1, 0.1, 0.1, 1);
    parser->carregarArquivo();
    criarObjeto(materialBrancoCinza);

    // Carregando Poste2 = 1 segundo
    parser = new Parser("../recursos/posteSimples2.obj");
    cenario.adicionaEsfera( Ponto(-4.1 , 5.5, 6.8), 0.15, materialLampada, true );
        // Lâmpada poste 1
    cenario.adicionaIluminacao(Ponto(-4.1 , 5.5, 6.8), 0.1, 0.1, 0.1, 1);
    parser->carregarArquivo();
    criarObjeto(materialBrancoCinza);

    // Carregando parte central = 35 segundos
    cenario.adicionaEsfera( Ponto(0.9 , 2.8, 0.4), 0.4, materialLampada, true );
        // Lâmpada globo central
    cenario.adicionaIluminacao(Ponto(0.9 , 2.8, 0.4), 0.7, 0.7, 0.7, 0.02);
    parser = new Parser("../recursos/pracaCentral.obj");
    parser->carregarArquivo();
    criarObjeto(materialBrancoCinza);

    // Carregando Calçada + Casas = 94 segundos
    parser = new Parser("../recursos/calcadaCasasSemLet.obj");
    parser->carregarArquivo();
    criarObjeto(materialBrancoCinza);


}

void MainWindow::carregarTeste(){

    Material materialVermelho( Cor(0.2, 0.0, 0.0), Cor(1.0, 0, 0), Cor(0.2, 0.2, 0.2), 200, 1.0, 1.0, 0.5 );
    Material materialVerde( Cor(0.0, 0.0, 0.2), Cor(0.0, 0, 1.0), Cor(0.2, 0.2, 0.2), 200, 1.0, 1.0, 0.5 );
    Material materialPrateado( Cor(0.19225, 0.19225, 0.19225), Cor(0.50754, 0.50754, 0.50754), Cor(0.508273, 0.508273, 0.508273), 0.4, 1.0, 1.0, 1.0 );
    Material materialBranco( Cor(0.2, 0.2, 0.2), Cor(1.0, 1.0, 1.0), Cor(1.0, 1.0, 1.0), 200, 1.0, 1.0, 0.05 );

    Material materialLampadaBranca(Cor(1, 1, 1),Cor(1, 1, 1),Cor(1, 1, 1),200, 1.0, 1.0, 0.05);
    Material materialLampadaAzul(Cor(0, 0, 1),Cor(0, 0, 1),Cor(0, 0, 1),200, 1.0, 1.0, 0.05);
    Material materialLampadaAmarela(Cor(1, 1, 0),Cor(1, 1, 0),Cor(1, 1, 0),200, 1.0, 1.0, 0.05);
    Material materialLampadaVerde(Cor(0, 1, 0),Cor(0, 1, 0),Cor(0, 1, 0),200, 1.0, 1.0, 0.05);

    //Esferas iluminação
    //branca
    cenario.adicionaEsfera( Ponto(-2 , 2, 2), 0.1, materialLampadaBranca, true );
    cenario.adicionaIluminacao(Ponto(-2 , 2, 2), 1.0, 1.0, 1.0, 0.0002);
    //verde
    cenario.adicionaEsfera( Ponto(2 , 2, -2), 0.1, materialLampadaVerde, true );
    cenario.adicionaIluminacao(Ponto(2 , 2, -2), 0.0, 1.0, 0.0, 0.0002);
    //amarela
    cenario.adicionaEsfera( Ponto(2 , 0, 2), 0.1, materialLampadaAmarela, true );
    cenario.adicionaIluminacao(Ponto(2 , 0, 2), 1.0, 1.0, 0.0, 0.0002);

    // Carregando base = 1 segundo
    parser = new Parser("../recursos/cubo.obj");
    parser->carregarArquivo();
    criarObjeto(materialPrateado);
    //transformador.transladar(cenario.objetosCenario[0], 5,1,1);
    //transformador.escalonar(cenario.objetosCenario[0], 5,3,1);

    //Esferas
    cenario.adicionaEsfera( Ponto(0, 1, 0), 0.3, materialVermelho, false );
    cenario.adicionaEsfera( Ponto(2, 0.5, -2), 1, materialVermelho, false );
    cenario.adicionaEsfera( Ponto(-2, 0.5, 2), 1, materialVerde, false );
    cenario.adicionaEsfera( Ponto(-1, 1, -1), 1, materialBranco, false );

}

/*---------------------------------------------------------------------------*
  Método que irá interagir e renderizar todos os objetos que pertencem ao
  cenário.
 *---------------------------------------------------------------------------*/
void MainWindow::renderizarCena(){

    /*------------------------------------------------------
      Pegamos os tamanhos da largura e altura do GraphicView
      e criamos uma imagem que irá receber os pixels a serem
      definidos...
    *------------------------------------------------------*/
    QSize janela =  ui->graphicsView->size();
    int alturaJanela = janela.height();
    int larguraJanela = janela.width();

    QImage imagem = QImage( larguraJanela, alturaJanela, QImage::Format_RGB32 );

    /*-------------------------------------------------------
      Iremos percorrer todos os pixels da imagem para atribuir
      um valor de cor para cada pixel.
      # de cima para baixo - da esquerda para direita)
    *-------------------------------------------------------*/
    for( int linha = 0 ; linha < alturaJanela; linha++){ // y
        for( int coluna = 0 ; coluna < larguraJanela; coluna++){ // x

            // cria um raio para cada posição do plano de projeção
            Ray ray = camera->lancarRaio(coluna,linha);

            Intersecao pInt;            

            // Verifica para todos os objetos do cenário, se o raio intercepta
            // algum ponto para ser pintado.
            if(cenario.intersecao(ray, pInt))
            {
                /*-------------------------------------------------
                  Aqui temos a certeza de um ponto interceptado
                  agora iremos calcular valores de iluminação, etc.
                 *------------------------------------------------*/
                Cor corPonto(0,0,0);
                corPonto = calcularCorAmbiente(pInt.material);

                /*---------------------------------------------------
                  Aqui iremos percorrer a lista de fontes de iluminação
                  do cenário para calcular as contribuições de cor,
                  sombra, etc.
                 *--------------------------------------------------*/
                if(!pInt.naoInterceptar){
                    for(int indiceLuz=0; indiceLuz < cenario.fontesIluminacao.size(); indiceLuz++)
                    {

                        Ponto pontoLuz = cenario.fontesIluminacao[indiceLuz].posicao;
                        bool temSombra = false;

                        // Origem do raio = ponto em questão
                        // Direção do raio = luz - ponto
                        pInt.ponto.y+=0.001; pInt.ponto.x+=0.001; pInt.ponto.z+=0.001;

                        Vetor direcaoRaioSombra = cenario.fontesIluminacao[indiceLuz].posicao - pInt.ponto;
                        direcaoRaioSombra = normalizarVetor(direcaoRaioSombra);
                        Ray raioSombra( pInt.ponto, direcaoRaioSombra );
                        Intersecao pIntSombra;
                        //Iremos verificar se o ponto está em sombra
                        //Precisamos percorrer todos os objetos
                        //
                        if(cenario.intersecao(raioSombra, pIntSombra)){
                            if((pIntSombra.distancia == 0)){

                                temSombra = false;
                            }else{
                                if((pIntSombra.naoInterceptar)){
                                    temSombra = false;
                                }else{
                                    temSombra = true;
                                }
                            }
                        }

                        if(!temSombra){

                            Cor corIluminacao = cenario.fontesIluminacao[indiceLuz].cor;

                            // Vetor L, V, R
                            Vetor L = cenario.fontesIluminacao[indiceLuz].posicao - pInt.ponto;
                            L.normalizar();
                            Vetor R = calcularRaioRefletido(L,pInt.normal);
                            Vetor V = (ray.origem.paraVetor() - ray.direcao);
                            V.normalizar();
                            float RV = produtoEscalar( V, R );

                            float atenuacaoDifusa = produtoEscalar(pInt.normal, L);

                            // Não há contribuição quando for negativo
                            if( atenuacaoDifusa < 0.0 )
                                atenuacaoDifusa = 0.0;
                            if( RV < 0.0  )
                                RV = 0.0;

                            //Cor difusa
                            corPonto = calcularCorDifusa(corPonto, corIluminacao ,pInt.material, atenuacaoDifusa);

                            //Cor Especular
                            corPonto = calcularCorEspecular(corPonto, corIluminacao ,pInt.material, RV);

                        }else{
                            // tem sombra, fica só a cor ambiente
                        }

                    }
                }
                // Comor a cor está entre [0,1], precisamos corrigir quem está fora disso
                normalizarCor(corPonto);

                // pinta o pixel com a cor definida
                imagem.setPixel( coluna, linha, qRgb( corPonto.r*255, corPonto.g*255, corPonto.b*255) );

            }
            else
            {
                //Caso não haja intersecao, pinta cor de background
                imagem.setPixel( coluna, linha, qRgb(10, 10, 10) );
            }

        }
    }

    /*--------------------------------------------------------
     Precisamos definir um GraphicsScene onde iremos mapear a
     imagem, e depois adicionar essa cena no GraphicsView.
    *--------------------------------------------------------*/
    QPixmap mapaPixel = QPixmap::fromImage(imagem);
    QGraphicsScene *cenarioMapeado = new QGraphicsScene(this);
    cenarioMapeado->addPixmap(mapaPixel);
    ui->graphicsView->setScene(cenarioMapeado);

}

void MainWindow::criarObjeto(Material material)
{
    // as faces devem ter o mesmo material do objeto   
    Objeto obj(material);

    for(int i=0 ; i < parser->vertices.size() ; i+=3){
        Ponto v1(parser->vertices[i].x ,parser->vertices[i].y ,parser->vertices[i].z );
        Ponto v2(parser->vertices[i+1].x ,parser->vertices[i+1].y ,parser->vertices[i+1].z );
        Ponto v3(parser->vertices[i+2].x ,parser->vertices[i+2].y ,parser->vertices[i+2].z );

        Triangulo face(v1,v2,v3, material);
        obj.adicionarFace(face);
    }

    cenario.adicionaObjeto(obj);
}


void MainWindow::adicionarSnowMan(){



    // Definição dos materiais
    // kamb, kdif, kespec, m, pka, pkd, pke
    Material materialVermelho( Cor(0.2, 0.0, 0.0), Cor(1.0, 0, 0), Cor(0.2, 0.2, 0.2), 200, 1.0, 1.0, 0.5 );
    Material materialBranco( Cor(0.2, 0.2, 0.2), Cor(1.0, 1.0, 1.0), Cor(1.0, 1.0, 1.0), 200, 1.0, 1.0, 0.05 );
    Material materialPreto( Cor(0.1, 0.1, 0.1), Cor(0.1, 0.1, 0.1), Cor(0.1, 0.1, 0.1), 200, 1.0, 1.0, 0.05 );
    Material materialCinza( Cor(0.4, 0.4, 0.4), Cor(0.1, 0.1, 0.1), Cor(0.1, 0.1, 0.1), 200, 1.0, 1.0, 0.05 );
    Material materialLampada(Cor(1, 1, 1),Cor(1, 1, 1),Cor(1, 1, 1),200, 1.0, 1.0, 0.05);

    // Corpo do snowman

    cenario.adicionaEsfera( Ponto(1,2,2), 0.2, materialBranco, false );
    cenario.adicionaEsfera( Ponto(1,-1,-2), 3, materialBranco, false );
/*
    //Balão
    cenario.adicionaEsfera( Ponto(0,0,-15), 7, materialBranco );

    // Cabeça do snowman
    cenario.adicionaEsfera( Ponto(0,5.5,0), 2, materialBranco );

    // Botões do snowman
    cenario.adicionaEsfera( Ponto(0,2,3.5), 0.5, materialVermelho );
    cenario.adicionaEsfera( Ponto(0,0,4), 0.5, materialVermelho );
    cenario.adicionaEsfera( Ponto(0,-2,3.5), 0.5, materialVermelho );

    // Olhos do snowman
    cenario.adicionaEsfera( Ponto(-0.8,6,1.7), 0.3, materialPreto );
    cenario.adicionaEsfera( Ponto(0.8,6,1.7), 0.3, materialPreto );
*/
    // Luz
    //cenario.adicionaIluminacao(Ponto(0, 10, -5), 1.0, 0.0, 0.0, 0.0001);
    //cenario.adicionaIluminacao(Ponto(0, 10, -10), 0.0, 0.0, 1.0, 0.0001);
    // Representação da luz
    //cenario.adicionaEsfera( Ponto(0,10,5), 0.1, materialBranco );

}

MainWindow::~MainWindow()
{
    delete ui;
}
