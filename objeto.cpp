#include "objeto.h"

Objeto::Objeto(Material material)
    : material(material)
{

}

void Objeto::adicionarFace(Triangulo f)
{
    f.material = this->material;
    this->faces.push_back(f);
}

void Objeto::adicionaMaterial(Material material)
{
    this->material = material;
}
