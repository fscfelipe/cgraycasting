#include "triangulo.h"

#include <iostream>
using namespace std;

Triangulo::Triangulo(Ponto vertice1, Ponto vertice2, Ponto vertice3, Material material)
    :v1(vertice1), v2(vertice2), v3(vertice3), material(material)
{

}

Vetor Triangulo::getNormal()
{
   Vetor v1v2 = v1 - v2;
   Vetor v1v3 = v1 - v3;
   Vetor N = produtoVetorial(v1v3, v1v2);

   return normalizarVetor(N);

}

Intersecao Triangulo::intersecao(Ray ray)
{

    Intersecao pInt;
    pInt.distancia = 0.0;

    Vetor normal = getNormal();
    Vetor origemRaio = ray.origem.paraVetor();

    Vetor OrigemV1(v1.x - origemRaio.x, v1.y - origemRaio.y, v1.z - origemRaio.z);

    // t ta um pouco mais correto
    float t = produtoEscalar(normal, OrigemV1) / produtoEscalar(normal, ray.direcao);;

     if (t < 0)
         return pInt;

     Ponto P = ray.origem + ray.direcao*t;

     // Step 2: inside-outside test
     Vetor C(0,0,0);

     // edge 1
     Vetor edge1 = v2 - v1;
     Vetor vp1 = P - v1;
     C = produtoVetorial(vp1, edge1);

     if (produtoEscalar(normal, C) < 0)
         return pInt;

     // edge 2
     Vetor edge2 = v3 - v2;
     Vetor vp2 = P - v2;
     C = produtoVetorial(vp2, edge2);
     if (produtoEscalar(normal, C) < 0)
         return pInt;

     // edge 3
     Vetor edge3 = v1 - v3;
     Vetor vp3 = P - v3;
     C = produtoVetorial(vp3, edge3);
     if (produtoEscalar(normal, C) < 0)
         return pInt;

     pInt.distancia = t;
     pInt.ponto = P;
     pInt.material = this->material;

     // Calculando normal a partir do ponto de interseção
     // encontrado.
     Vetor PintV3 = v3 - P;
     Vetor PintV2 = v2 - P;
     Vetor normalPint = produtoVetorial(PintV2,PintV3);
     normalPint = normalizarVetor(normalPint);
     //cout << normalPint.x << ", " <<normalPint.y << ", " << normalPint.z << endl;
     pInt.normal  = normalPint;


     return pInt;

}
