#include "cenario.h"
#include <iostream>
#include "triangulo.h"
using namespace std;

Cenario::Cenario()
{

}

/*----------------------------------------------------------
 Para todo objeto no cenário verificamos se há interseção
*----------------------------------------------------------*/
bool Cenario::intersecao( Ray &ray, Intersecao &intersecao)
{
    // Variável distância utilizada para verificar o primeiro ponto
    // interceptado, que será então atualizada;
    float distancia = 9999999;
    Intersecao pIntMaisProximo;

    // Para cada esfera adicionada no cenário, verificamos se há interceção
    // e retornamos o ponto mais próximo dentre todos os pontos interceptados

    for(auto const& esfera: esferas ){
            Intersecao pInt = esfera->intersecao(ray);

            if( pInt.distancia > 0.0 && pInt.distancia <= distancia )
            {
                distancia = pInt.distancia;
                pIntMaisProximo = pInt;

                if(esfera->naoInterceptar){
                    pIntMaisProximo.naoInterceptar = true;
                }

            }

    }

    for(auto const& objeto: objetosCenario ){
        for(int i=0 ; i < objeto.faces.size() ; i++ ){

            Triangulo face = objeto.faces[i];
            /*cout << objeto.faces[i].v1.x << objeto.faces[i].v1.y << objeto.faces[i].v1.z << endl;
            cout << objeto.faces[i].v2.x << objeto.faces[i].v2.y << objeto.faces[i].v2.z << endl;
            cout << objeto.faces[i].v3.x << objeto.faces[i].v3.y << objeto.faces[i].v3.z << endl;
            */Intersecao pInt = face.intersecao(ray);

            if( pInt.distancia > 0.0 && pInt.distancia <= distancia )
            {
                distancia = pInt.distancia;
                pIntMaisProximo = pInt;
            }
        }
    }

    // Não houve interseção
    if ( distancia == 9999999)
        return false;

    intersecao = pIntMaisProximo;

    return true;

}


void Cenario::adicionaIluminacao( const Ponto &posicao, float r, float g, float b, float atenuacao)
{
    Iluminacao luz;
    luz.cor.setCor(r, g, b);
    luz.posicao = posicao;
    luz.atenuacao = atenuacao;
    fontesIluminacao.push_back(luz);
}

void Cenario::adicionaEsfera(const Ponto& centro, float raio, Material material, bool naoIterceptar)
{
    Esfera *novaEsfera = new Esfera(centro, raio);
    novaEsfera->naoInterceptar = naoIterceptar;
    novaEsfera->material=material;
    esferas.push_back(novaEsfera);
}

void Cenario::adicionaObjeto(const Objeto &obj)
{
    objetosCenario.push_back(obj);
}
