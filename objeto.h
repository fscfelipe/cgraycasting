#ifndef OBJETO_H
#define OBJETO_H

#include "triangulo.h"
#include <vector>

using namespace std;

class Objeto
{
public:

    vector<Triangulo> faces;
    Material material;

    Objeto(Material material);
    void adicionarFace(Triangulo f);
    void adicionaMaterial(Material material);

};

#endif // OBJETO_H
