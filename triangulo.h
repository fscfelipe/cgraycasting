#ifndef TRIANGULO_H
#define TRIANGULO_H

#include "intersecao.h"

class Triangulo
{
public:

    Ponto v1, v2, v3;
    Material material;
    Triangulo(Ponto vertice1, Ponto vertice2, Ponto vertice3, Material material);

    Vetor getNormal();
    Intersecao intersecao(Ray ray);
};

#endif // TRIANGULO_H
