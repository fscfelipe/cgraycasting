#ifndef CENARIO_H
#define CENARIO_H

#include "esfera.h"
#include "iluminacao.h"
#include "objeto.h"
#include <vector>

using namespace std;

/*----------------------------------------------
  Definição do cenário que guarda todos os objetos
  e iluminações. É a partir do cenário que iremos
  percorrer todos os objetos para verificar se há
  interseção com o raio...
  *--------------------------------------------*/

class Cenario
{
public:

    vector<Esfera*> esferas;
    vector<Iluminacao> fontesIluminacao;
    vector<Objeto> objetosCenario;

    Cenario();

    bool intersecao( Ray& ray, Intersecao &intersecao);
    void adicionaIluminacao( const Ponto& centro, float r, float g, float b, float atenuacao);
    void adicionaEsfera(const Ponto& centro, float raio, Material material, bool naoIterceptar);
    void adicionaObjeto(const Objeto& obj);

};

#endif // CENARIO_H
