#include "vetor.h"

Vetor::Vetor(float coordX, float coordY, float coordZ)
{
    this->x = coordX;
    this->y = coordY;
    this->z = coordZ;
}

Vetor Vetor::operator+(const Vetor &v) const
{
    return Vetor((this->x + v.x),(this->y + v.y),(this->z + v.z));
}

Vetor Vetor::operator-(const Vetor &v) const
{
    return Vetor((this->x - v.x),(this->y - v.y),(this->z - v.z));
}

Vetor Vetor::operator*(float f)
{
    return Vetor((this->x * f),(this->y * f),(this->z * f));
}

Vetor Vetor::operator/(float f)
{
    return Vetor((this->x / f),(this->y / f),(this->z / f));
}

float Vetor::modulo()
{
     return sqrt(x*x + y*y + z*z);
}

void Vetor::normalizar() {
    float modulo = this->modulo();
    x = x / modulo;
    y = y / modulo;
    z = z / modulo;
}

float produtoEscalar(Vetor &v1, Vetor &v2)
{
    return (v1.x * v2.x) + (v1.y * v2.y) + (v1.z * v2.z);
}

Vetor produtoVetorial(Vetor &v1, Vetor &v2)
{
    return Vetor((v1.y*v2.z) - (v1.z*v2.y),
                 (v1.z*v2.x) - (v1.x*v2.z),
                 (v1.x*v2.y) - (v1.y*v2.x));
}

Vetor normalizarVetor(Vetor &vetor)
{
    float modulo = vetor.modulo();
    float x = vetor.x / modulo;
    float y = vetor.y / modulo;
    float z = vetor.z / modulo;

    return Vetor(x,y,z);
}
