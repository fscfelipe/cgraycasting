#ifndef TRANSFORMACOES_H
#define TRANSFORMACOES_H

#include "objeto.h"


class Transformacoes
{
public:
    Transformacoes();
    void rotacaoZ(Objeto &objeto, float angulo);
    void rotacaoY(Objeto &objeto, float angulo);
    void rotacaoX(Objeto &objeto, float angulo);
    void escalonar(Objeto &objeto, float sx, float sy, float sz);
    void transladar(Objeto &objeto, float tx, float ty, float tz);

};

#endif // TRANSFORMACOES_H
