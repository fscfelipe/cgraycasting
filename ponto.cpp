#include "ponto.h"

Ponto::Ponto(float coordX, float coordY, float coordZ)
{
    this->x = coordX;
    this->y = coordY;
    this->z = coordZ;
}

Ponto Ponto::operator+( const Vetor &v) const
{
    return Ponto((this->x + v.x),(this->y + v.y), (this->z + v.z));
}

Ponto Ponto::operator+(const Ponto &p) const
{
    return Ponto((this->x + p.x),(this->y + p.y), (this->z + p.z));
}

Ponto Ponto::operator-( const Vetor &v) const
{
    return Ponto((this->x - v.x),(this->y - v.y), (this->z - v.z));
}

Ponto Ponto::operator/(float div)
{
    return Ponto((this->x / div),(this->y / div), (this->z / div));
}

Vetor Ponto::operator-( const Ponto& p) const
{
    return Vetor(this->x - p.x, this->y - p.y, this->z - p.z);
}

Vetor Ponto::paraVetor()
{
    return Vetor(this->x, this->y, this->z);
}

