#include "parser.h"
#include <iostream>

Parser::Parser(const char *path): path(path)
{

}

bool Parser::carregarArquivo()
{

    /*------------------------------------------
       Carregando arquivo
     *----------------------------------------*/
    FILE * file = fopen(path, "r");
    if( file == NULL ){
        perror("Erro ao abrir arquivo;");
        return false;
    }

    /*-------------------------------------------
       Percorrendo o arquivo até encontrar EOF
     *-----------------------------------------*/
    while( 1 ){

        /*----------------------------------------------------
          O cabeçalho da linha indica com qual atríbuto do
          objeto estamos lidando, seja, vértice, face, etc.
          *------------------------------------------------*/
        char cabecalhoLinha[128];
        int res = fscanf(file, "%s", cabecalhoLinha);
        if (res == EOF)
            break;

        // Lendo vértices
        if ( strcmp( cabecalhoLinha, "v" ) == 0 ){

            Ponto vertex(0,0,0);
            fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z );
            tempVertices.push_back(vertex);

        // Lendo coordenadas de textura UV
        }else if ( strcmp( cabecalhoLinha, "vt" ) == 0 ){
            Ponto uv(0,0,0);
            fscanf(file, "%f %f\n", &uv.x, &uv.y );
            tempUVs.push_back(uv);

        // Lendo normais dos vértices
        }else if ( strcmp( cabecalhoLinha, "vn" ) == 0 ){
            Vetor normal(0,0,0);
            fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
            tempNormais.push_back(normal);


        // Lendo faces (índices para seus respectivos vértices...)
        }else if ( strcmp( cabecalhoLinha, "f" ) == 0 ){

            /*
             * OBS.: RETIRADO O CARREGAMENTO DOS UVS
             */
            unsigned int verticeIndice[3], uvIndice[3], normalIndice[3];
            int matches = fscanf(file, "%d//%d %d//%d %d//%d\n",
                                 &verticeIndice[0],  &normalIndice[0],
                                 &verticeIndice[1],  &normalIndice[1],
                                 &verticeIndice[2],  &normalIndice[2] );

            if (matches != 6){
                printf("ERRO ! \n");
                return false;
            }

            verticeIndices.push_back(verticeIndice[0]);
            verticeIndices.push_back(verticeIndice[1]);
            verticeIndices.push_back(verticeIndice[2]);
            /*
            uvIndices.push_back(uvIndice[0]);
            uvIndices.push_back(uvIndice[1]);
            uvIndices.push_back(uvIndice[2]);
            */
            normalIndices.push_back(normalIndice[0]);
            normalIndices.push_back(normalIndice[1]);
            normalIndices.push_back(normalIndice[2]);
        }



    }


    /*-----------------------------------------------------------------
       Iremos percorrer a lista de indices dos vértices para podermos ordenar
       uma lista contendo os vértices ordenados por face.
       ex Face 1 são os 3 primeiros vértices da lista
          Face 2, vértice 4,5,6
          e assim por diante, o mesmo deve ser feito para os UVs e normais.
     *-----------------------------------------------------------------*/
    for( unsigned int i=0; i<verticeIndices.size(); i++ ){
        unsigned int indiceVertice = verticeIndices[i];
        Ponto vertice = tempVertices[ indiceVertice-1 ];
        vertices.push_back(vertice);
    }

    fclose(file);

}
