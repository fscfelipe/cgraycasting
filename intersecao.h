#ifndef INTERSECAO_H
#define INTERSECAO_H

#include "ponto.h"
#include "material.h"
#include "ray.h"

/*------------------------------------------------
 Componente utilizado para guardar toda informação
 do ponto de interseção interceptado pelo raio
 facilitando os cálculos do raycasting.
 *-----------------------------------------------*/
class Intersecao
{
public:

    Ponto ponto;
    Vetor normal;
    Material material;
    float distancia;
    bool naoInterceptar = false;

    Intersecao();

};

#endif // INTERSECAO_H
