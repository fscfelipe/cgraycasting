#include "transformacoes.h"

Transformacoes::Transformacoes()
{

}

void Transformacoes::rotacaoX(Objeto &objeto, float angulo)
{



}

void Transformacoes::escalonar(Objeto &objeto, float sx, float sy, float sz)
{

    for(int i=0 ; i < objeto.faces.size() ; i++){

        objeto.faces[i].v1.x *= sx;
        objeto.faces[i].v1.y *= sy;
        objeto.faces[i].v1.z *= sz;

        objeto.faces[i].v2.x *= sx;
        objeto.faces[i].v2.y *= sy;
        objeto.faces[i].v2.z *= sz;

        objeto.faces[i].v3.x *= sx;
        objeto.faces[i].v3.y *= sy;
        objeto.faces[i].v3.z *= sz;

    }

}

void Transformacoes::transladar(Objeto &objeto, float tx, float ty, float tz)
{


    for(int i=0 ; i < objeto.faces.size() ; i++){

        objeto.faces[i].v1.x += tx;
        objeto.faces[i].v1.y += ty;
        objeto.faces[i].v1.z += tz;

        objeto.faces[i].v2.x += tx;
        objeto.faces[i].v2.y += ty;
        objeto.faces[i].v2.z += tz;

        objeto.faces[i].v3.x += tx;
        objeto.faces[i].v3.y += ty;
        objeto.faces[i].v3.z += tz;

    }
}
